1.1.0 - 2017-05-17 
--------------------------------

- Primeira versão marcada após o Fork.
  [luiz.lgsb]

1.0.0 - 2017-05-16 (unreleased)
--------------------------------

- Fork de pf.internet.policy 
  [luiz.lgsb]
