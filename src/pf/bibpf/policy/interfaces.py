#-*- coding: utf-8 -*-

from zope.interface import Interface


class IAddonSpecific(Interface):
    """
    Marker interface that defines a browser layer against which you can register views and viewlets.
    When your add-on is installed this layer becomes active and all overrides associated with
    override default Plone render actions.
    """
